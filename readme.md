## Brownian Emotion


This project is work in progress with goal to study the impact of political returns on stock markets.

Next task is to build a 3 dimensional time series of variance covariance  matrix of barra risk factors and look at the performance of winning signal.
 
Emotion: main package
    - It has three primary packages:
    
    - lib: library for time series analysis and backtesting.
    
    - raw: modules for parsing raw data.
    
    - genesis: modules for testing hypothesis and ideas.


empirics: 
    - contains the backtesting result for signal

hub: 
    - raw and derived timeseries of signals, prices etc


### Dev setup
1. Create isolated environment to avoid compatibility issues  with global python packages.
Create a new conda environment with given setup. To install conda follow this
[link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)

```bash
#create env
cd brownian-emotion
conda env create -f environment.yml
source activate be-env


```
As, yfinance is not available with conda, use pip to install it

```bash
pip install yfinance
```

### Run instructions

cd into brownian-emotion

```bash
python -m emotion
```

Above command will generate empirics for all signals and also create all data items.


## Signal

We have one simple signal, securities are divided into two groups, one democratic and other 