"""
presidential debate nlp factors
"""

# core modules
import pandas as pd
import os
# local modules
import emotion
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever
import statsmodels.api as sm
import emotion.lib.tsa as tsa

from pandas.tseries.offsets import BDay

def get_score_factor(name, start, end):
    dfscores = pd.read_csv(os.path.join(emotion.ROOT_DIR, '../external_data/scores.csv'), skiprows=1)
    dfscores.index = pd.to_datetime(dfscores['date'], utc=True)
    dfscores.index = dfscores.index.rename(ts.DATENAME)
    score_type = "vote_match"
    if(score_type == "ideological"):
        score_col_selector = ['p0','p1','p2','p3','p4','p4','p6','p7','p8','p9','p10','p11','p12']
    elif (score_type == "biographical"):
        score_col_selector = ['b1','b1','b2','b4','b4','b5','b6','b7','b8','b9','b10','b11','b12']
    elif(score_type == "vote_match"):
        score_col_selector = ['s1','s2','s3','s4','s5','s6','s7','s8','s9','s10','e1','e2','e3','e4','e5','e6','e7','e8','e9','e10','p0']
    dfccs_factor = dfscores.copy()
    dfccs_factor = dfccs_factor[score_col_selector]
    # Agg score is simple sum/mean
    dfccs_factor['agg_score'] = dfccs_factor.sum(axis=1)
    dfccs_factor = ts.date_filter(dfccs_factor, start, end)
    return dfccs_factor

@stash_to_hub
def get_stashed_score_factor(name, start, end):
    if '.factor' in name:
        return get_score_factor(name, start, end)

def get(name, start_date, end_date, **kwargs):
    return get_stashed_score_factor(name, start_date, end_date, **kwargs)


if __name__ == '__main__':
    start = pd.Timestamp('19930101', tz='UTC')
    end = pd.Timestamp('20191231', tz='UTC')
    
    name_ccs = 'emotion.genesis.ccs.factor'
    df = retriever.get(name_ccs, start, end)
    dfccs_factor = df[['agg_score']]
    df_nominate = df[['p0']]
    df_nominate = df_nominate.rename(columns={
            'p0': 'DW_Nominate'
            })
    df_nominate = df_nominate.asfreq('BM', method='ffill')
    dfccs_factor = pd.read_csv(os.path.join(emotion.ROOT_DIR, '../external_data/ccs.csv'))
    dfccs_factor = dfccs_factor[['DATE', 'agg_score']]
    dfccs_factor = dfccs_factor.set_index('DATE')
    dfccs_factor.index = pd.to_datetime(dfccs_factor.index, utc=True)
    dfccs_factor = ts.date_filter(dfccs_factor, start, end)
    dfccs_factor = dfccs_factor.asfreq('BM', method='ffill')
    signal_analytics = tsa.TSA(label='emotion.genesis.score_factor')
    # Republican and Democratic portfolio returns
    dfrep = retriever.get('emotion.genesis.portfolio.republican', start, end)
    dfdem = retriever.get('emotion.genesis.portfolio.democratic', start, end)
    dffundamental = retriever.get('emotion.raw.factor.fundamental', start, end)
    dffundamental = dffundamental.drop('RF', axis=1)
    dfh_rep = signal_analytics.get_holdings(dfrep)
    dfh_dem = signal_analytics.get_holdings(dfdem)
    
    # Raw Returns
    dfret = retriever.get('emotion.raw.return', start, end )
    df_dem_ret = signal_analytics.get_returns(dfh_dem, dfret)
    df_rep_ret = signal_analytics.get_returns(dfh_rep, dfret)
    
#    df_dem_ret_annual = (1+df_dem_ret).cumprod().resample('BM').last().pct_change().dropna()
#    df_rep_ret_annual = (1+df_rep_ret).cumprod().resample('BM').last().pct_change().dropna()
#    dffundamental_annual =  (1+dffundamental).cumprod().resample('BM').last().pct_change().dropna()
#    # dem ols regression
#    dfx_dem = pd.concat([dffundamental, df_nominate], axis=1).dropna()
#    dfy_dem = df_dem_ret_annual.loc[dfx_dem.index]
#    dem_res = sm.OLS(dfy_dem, dfx_dem).fit()
#    
##    res = dfy_dem.values.reshape(1, -1)- dem_res.predict(dfx_dem).values.reshape(1, -1)
#    
#    dfx_rep = pd.concat([dffundamental, df_nominate], axis=1).dropna()
#    dfy_rep = df_rep_ret_annual
#    dfy_rep = dfy_rep.loc[dfx_rep.index]
#    rep_res = sm.OLS(dfy_rep, dfx_rep).fit()
    
    
    
    ## grand regression
    
    df_dem_ret = (1+df_dem_ret).cumprod().resample('BM').last().pct_change()
    df_rep_ret = (1+df_rep_ret).cumprod().resample('BM').last().pct_change()
#    df_dem_ret = df_dem_ret.shift(-1).dropna()
#    df_rep_ret = df_rep_ret.shift(-1).dropna()
    dates = pd.date_range(start, end, freq='BM')
    dfccs_factor = dfccs_factor.copy()
    dates = pd.date_range(start, end, freq='BM')
    dfcampaign = retriever.get('emotion.genesis.campaign_factor', start, end)
    dfcampaign = dfcampaign.reindex(dates, method='ffill', limit=1).dropna()
    dffundamental = retriever.get('emotion.raw.factor.fundamental', start, end)
    dffundamental = dffundamental.drop('RF', axis=1)
    dffundamental = dffundamental[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
    dfpol = retriever.get('emotion.genesis.political_var', start, end)
    dfpol = dfpol[['House Gridlock', 'House-President Gridlock', 'Democratic']]
    df_dem_ret = df_dem_ret.reindex(dfcampaign.index)[1:]
    df_rep_ret = df_rep_ret.reindex(dfcampaign.index)[1:]
    dfcampaign = dfcampaign[1:]
    dffundamental = dffundamental.reindex(dfcampaign.index, method='ffill')
    dfpol = dfpol.reindex(dfcampaign.index, method='ffill')
    dfccs_factor = dfccs_factor.reindex(dfcampaign.index, method='ffill')
    # freq
#    dfccs_factor = dfccs_factor
#    dffundamental =  (1+dffundamental).cumprod().resample('BM').last().pct_change()
#    dfccs_factor = dfccs_factor.reindex(dates, method='ffill')
#    dfpol = dfpol.reindex(dates, method='ffill')
#    df_rep_ret = df_rep_ret.reindex(dfcampaign.index)
#    dfcampaign.iloc[:,:7]
    dfcomp = pd.concat([dffundamental, dfccs_factor, dfpol, dfcampaign.iloc[:,:7] ], axis=1).dropna()
    dfcomp = dfcomp.rename(columns={'agg_score': 'CCS'})
    reg_res = sm.OLS(df_rep_ret, dfcomp).fit()
    
    dem_res = sm.OLS(df_dem_ret , dfcomp).fit()
    
    out_res = sm.OLS(df_rep_ret - df_dem_ret, dfcomp).fit()
    
    candidate_score = pd.read_pickle(os.path.join(emotion.ROOT_DIR, '../external_data/candidate_2020_scores.pickle'))
    party_score = pd.read_pickle(os.path.join(emotion.ROOT_DIR, '../external_data/party_2020_scores.pickle'))

    # trump wins
   
    dfy = df_rep_ret - df_dem_ret
    dfydem = df_dem_ret
    dfyrep = df_rep_ret

    
    candidates = ['trump', 'biden', 'buttigieg', 'amy', 'bernie', 'warren']
    
    lasso_dem= Lasso(alpha=.0001).fit(dfcomp, dfydem.values.ravel())
    lasso_score = lasso_dem.score(dfcomp, dfydem.values.ravel())
    print('dem lasso : ', lasso_score)
    lasso_rep= Lasso(alpha=.0001).fit(dfcomp, dfyrep.values.ravel())
    lasso_score = lasso_rep.score(dfcomp, dfyrep.values.ravel())
    print('rep lasso : ', lasso_score)

    
    lasso_longshot= Lasso(alpha=.0001).fit(dfcomp, dfy.values.ravel())
    lasso_score = lasso_longshot.score(dfcomp, dfy.values.ravel())
    print('rep - dem lasso : ', lasso_score)
#    for c in candidates:
#        if c=='trump':
#            dem = 0
#        else:
#            dem = 1
#        x = [1, 0, dem, *list(candidate_score.loc[c, :].values)]
#        print(c, lasso_reg_rep.predict([x])*3)
#    
#    
#    x = [1, 0, 0, *list(party_score.loc['repub', :].values)]
#    print('repub wins', lasso_reg_rep.predict([x])*3)
#    x = [1, 0, 1, 0.03436474, 0.13858227, 0.31742226, 0.13490154, 0.14994363,
#       0.15758719, 0.12719837]
#    print('dem wins', lasso_reg_rep.predict([x])*3)
    
    fig, ax = plt.subplots(figsize=(22, 18))
    width = 0.3
    ax.bar(np.arange(len(lasso_rep.coef_)), lasso_rep.coef_, width,
           label='Republican Portfolio')
    ax.bar(np.arange(len(lasso_dem.coef_)) - width, lasso_dem.coef_, width,
           label='Democratic Portfolio')

    ax.set_xticks(np.arange(len(lasso_rep.coef_)))
    ax.set_xticklabels(dfcomp.columns, rotation=90)
    ax.set_title('Lasso Regression on Political Factors')
    ax.set_xlabel('Factor')
    ax.set_ylabel('Lasso Coefficient')
    ax.legend(loc='best')
    fig.savefig('/Users/rohitchandel/Documents/be_results/pol_lasso.png')
    
    
    fig, ax = plt.subplots(figsize=(22, 18))
    width = 0.3
    ax.bar(np.arange(len(lasso_longshot.coef_)), lasso_longshot.coef_, width,
           label='Republican - Democratic ')
    

    ax.set_xticks(np.arange(len(lasso_longshot.coef_)))
    ax.set_xticklabels(dfcomp.columns, rotation=90)
    ax.set_title('Lasso Regression on Political Factors')
    ax.set_xlabel('Factor')
    ax.set_ylabel('Lasso Coefficient')
    ax.legend(loc='best')
    fig.savefig('/Users/rohitchandel/Documents/be_results/pol_longshort_lasso.png')

    

    
    
    
    