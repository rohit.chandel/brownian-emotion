import pandas as pd
import os
# local modules
import emotion
import emotion.lib.ts as ts
import emotion.lib.utils as utils
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever
from sklearn.linear_model import Lasso
import statsmodels.api as sm
import emotion.lib.tsa as tsa
import numpy as np
import matplotlib.pyplot as plt


def get_campaign_factor(name, start, end):
    dfcamp = pd.read_pickle(os.path.join(emotion.ROOT_DIR,
                                            '../external_data/dem_port_campaign_15.pickle'))
    dfcamp.index = pd.to_datetime(dfcamp[1], utc=True)
    dfcamp = dfcamp.iloc[:, 2:16]
    
    for i, col in enumerate(dfcamp.columns):
        dfcamp = dfcamp.rename(columns={
                col: 'CampaignFactor_'+str(i+1)
                })
    dfcamp.index = dfcamp.index.rename(ts.DATENAME)
    dfcamp = dfcamp.sort_index(axis=0)
    dfcamp = ts.date_filter(dfcamp, start, end)
    return dfcamp


@stash_to_hub
def get_stashd_campaign_factor(name, start, end):
    return get_campaign_factor(name, start, end)


def get(name, start_date, end_date, **kwargs):
    return get_stashd_campaign_factor(name, start_date, end_date, **kwargs)


if __name__ == '__main__':
    start = pd.Timestamp('19991231', tz='UTC')
    end = pd.Timestamp('20191231', tz='UTC')
    name_dem = 'emotion.genesis.campaign_factor'
    name_rep = 'emotion.genesis.campaign_factor'
    dfdem_factors = retriever.get(name_dem, start, end)
    dfrep_factors = retriever.get(name_rep, start, end)
    dfdem_factors = dfdem_factors.asfreq(freq='BM', method='ffill')
    dfrep_factors = dfrep_factors.asfreq(freq='BM', method='ffill')

    signal_analytics = tsa.TSA(label='emotion.genesis.campaign_factor')
    dfrep = retriever.get('emotion.genesis.portfolio.republican', start, end)
    dfdem = retriever.get('emotion.genesis.portfolio.democratic', start, end)
    
    dfh_rep = signal_analytics.get_holdings(dfrep)
    dfh_dem = signal_analytics.get_holdings(dfdem)
    dfret = retriever.get('emotion.raw.return', start, end )
    df_dem_ret = signal_analytics.get_returns(dfh_dem, dfret)
    df_rep_ret = signal_analytics.get_returns(dfh_rep, dfret)
    dates = pd.date_range(start, end, freq='B')
    df_dem_ret = (1+df_dem_ret).cumprod().resample('BM').last().pct_change().dropna()
    df_rep_ret = (1+df_rep_ret).cumprod().resample('BM').last().pct_change().dropna()
    
    dffundamental = retriever.get('emotion.raw.factor.fundamental', start, end)
    dffundamental = dffundamental.drop('RF', axis=1)
    dffundamental = dffundamental[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
    dffundamental = (1+dffundamental).cumprod().resample('BM').last().pct_change().dropna()
    dfdem_factors = dfdem_factors.dropna(how='all')
#    
    dfrep_factors = dfrep_factors.dropna(how='all')
    
#    dfrep_factors = dfcombined_factors
#    dfdem_factors = dfcombined_factors
    # dem ols regression
    dfy_dem = df_dem_ret
    dfy_dem = dfy_dem.dropna()
    dffundamental_dem = dffundamental.reindex(dfdem_factors.index)
    dfdem_factors = pd.concat([dffundamental, dfdem_factors], axis=1)
    dfdem_factors = dfdem_factors.reindex(dfy_dem.index)
    dfdem_factors = dfdem_factors.fillna(0)
    dem_res = sm.OLS(dfy_dem, dfdem_factors).fit()
    
#    lasso_reg_dem = Lasso(alpha=.00001).fit(dfdem_factors, dfy_dem.values.ravel())
#    
#    lasso_reg_dem_score = lasso_reg_dem.score(dfdem_factors, dfy_dem.values.ravel())
    # rep ols regression
    
    dfy_rep = df_rep_ret
    dfy_rep = dfy_rep.dropna()
    dffundamental_rep = dffundamental.reindex(dfrep_factors.index)
    dfrep_factors = pd.concat([dffundamental, dfrep_factors], axis=1)
    dfrep_factors = dfrep_factors.reindex(dfy_rep.index)
    dfrep_factors = dfrep_factors.fillna(0)
    rep_res = sm.OLS(dfy_rep, dfrep_factors).fit()
    dfcomb = pd.concat([dfrep_factors, dfdem_factors], axis=1)
    dfcomb = dfcomb.drop(['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA'], axis=1)
    out_res = sm.OLS(dfy_rep - dfy_dem, dfcomb).fit()
#    lasso_reg_rep= Lasso(alpha=.0001).fit(dfrep_factors, dfy_rep.values.ravel())
#    lasso_score_rep_score = lasso_reg_rep.score(dfrep_factors, dfy_rep.values.ravel())
#    
#    dflasso_rep = pd.DataFrame(np.array([lasso_reg_rep.coef_]),
#                                  columns=dfrep_factors.columns)
#    
#    dflasso_dem = pd.DataFrame(np.array([lasso_reg_dem.coef_]),
#                                  columns=dfdem_factors.columns)
#    fig, ax = plt.subplots(figsize=(20,10))
#    ax.bar(range(len(lasso_reg_rep.coef_)), lasso_reg_rep.coef_)
#    ax.set_xticks(range(len(lasso_reg_rep.coef_)))
#    ax.xaxis.set_tick_params(rotation=45)
#    ax.set_xticklabels(dfrep_factors.columns, rotation=45)
#    fig.savefig('/Users/rohitchandel/Documents/be_results/camprep_lasso.png')
#
#    fig, ax = plt.subplots(figsize=(20,10))
#    ax.bar(range(len(lasso_reg_dem.coef_)), lasso_reg_dem.coef_)
#    ax.set_xticks(range(len(lasso_reg_dem.coef_)))
#    ax.set_xticklabels(dfdem_factors.columns, rotation=45)
#    fig.savefig('/Users/rohitchandel/Documents/be_results/campdem_lasso.png')
