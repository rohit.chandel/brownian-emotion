"""
presidential debate nlp factors
"""

# core modules
import pandas as pd
import os
# local modules
import emotion
import emotion.lib.ts as ts
import emotion.lib.utils as utils
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever
from sklearn.linear_model import Lasso
import statsmodels.api as sm
import emotion.lib.tsa as tsa
import numpy as np
import matplotlib.pyplot as plt

def get_debate_factor(name, start, end):
    dfnlp_factors = pd.read_pickle(os.path.join(emotion.ROOT_DIR,
                                            '../external_data/dem_port_pca.pickle'))
    if '.dem' in name:
        key = 'dem'
    else:
        key = 'rep'
    df_nlp = dfnlp_factors[dfnlp_factors[0] == key].copy()

    df_nlp.index = pd.to_datetime(df_nlp[1], utc=True)
    df_nlp = df_nlp.iloc[:, 2:32]
    
    for i, col in enumerate(df_nlp.columns):
        df_nlp = df_nlp.rename(columns={
                col: 'PC'+str(i+1)
                })
    df_nlp.index = df_nlp.index.rename(ts.DATENAME)
    df_nlp = df_nlp.sort_index(axis=0)
    df_nlp = ts.date_filter(df_nlp, start, end)
    return df_nlp

@stash_to_hub
def get_stashd_debate_factor(name, start, end):
    return get_debate_factor(name, start, end)


def get(name, start_date, end_date, **kwargs):
    return get_stashd_debate_factor(name, start_date, end_date, **kwargs)


if __name__ == '__main__':
    start = pd.Timestamp('20001231', tz='UTC')
    end = pd.Timestamp('20200204', tz='UTC')
    name_dem = 'emotion.genesis.debate_factor.dem'
    name_rep = 'emotion.genesis.debate_factor.rep'
    dfdem_factors = retriever.get(name_dem, start, end)
    dfrep_factors = retriever.get(name_rep, start, end)
    signal_analytics = tsa.TSA(label='emotion.genesis.debate_factor')
    dfrep = retriever.get('emotion.genesis.portfolio.republican', start, end)
    dfdem = retriever.get('emotion.genesis.portfolio.democratic', start, end)
    dfh_rep = signal_analytics.get_holdings(dfrep)
    dfh_dem = signal_analytics.get_holdings(dfdem)
    dfret = retriever.get('emotion.raw.return', start, end )
    df_dem_ret = signal_analytics.get_returns(dfh_dem, dfret)
    df_rep_ret = signal_analytics.get_returns(dfh_rep, dfret)
    dates = pd.date_range(start, end, freq='B')
    
    dffundamental = retriever.get('emotion.raw.factor.fundamental', start, end)

    
    dfcombined_factors = pd.concat([dfrep_factors, dfdem_factors], axis=0)
    
#    dfrep_factors = dfcombined_factors
#    dfdem_factors = dfcombined_factors
    # dem ols regression
    
    
    # rep ols regression
    rep_scores = []
    dem_scores = []
    
    dfdem_factors = dfdem_factors.reindex(dates, method='ffill', limit=1)
    dfdem_factors = dfdem_factors.dropna(how='all')
    dfrep_factors = dfrep_factors.reindex(dates, method='ffill', limit=1)
    dfrep_factors = dfrep_factors.dropna(how='all')
    dfy_dem = df_dem_ret.shift(-1)
    dfy_dem = dfy_dem.reindex(dfdem_factors.index)
    dffundamental_dem = dffundamental.reindex(dfdem_factors.index)
    dfdem_factors = pd.concat([dfdem_factors, dffundamental_dem], axis=1)
    dfdem_factors = dfdem_factors.fillna(0)
    dfy_dem = dfy_dem.fillna(0)

    dem_res = sm.OLS(dfy_dem, dfdem_factors).fit()
    
    lasso_reg_dem = Lasso(alpha=.0001).fit(dfdem_factors, dfy_dem.values.ravel())
    
    lasso_reg_dem_score = lasso_reg_dem.score(dfdem_factors, dfy_dem.values.ravel())
    dem_scores.append(lasso_reg_dem_score)
    
    dfy_rep = df_rep_ret.shift(-1)
    dfrep_factors = dfrep_factors.fillna(0)
    dfy_rep = dfy_rep.reindex(dfrep_factors.index)
    dffundamental_rep = dffundamental.reindex(dfrep_factors.index)
    dfrep_factors = pd.concat([dfrep_factors, dffundamental_rep], axis=1)
    dfrep_factors = dfrep_factors.fillna(0)
    dfy_rep = dfy_rep.fillna(0)

    rep_res = sm.OLS(dfy_rep, dfrep_factors).fit()
    
    lasso_reg_rep= Lasso(alpha=.0001).fit(dfrep_factors, dfy_rep.values.ravel())
    lasso_score_rep_score = lasso_reg_rep.score(dfrep_factors, dfy_rep.values.ravel())
    rep_scores.append(lasso_score_rep_score)

    dflasso_rep = pd.DataFrame(np.array([lasso_reg_rep.coef_]),
                                  columns=dfrep_factors.columns)
    
    dflasso_dem = pd.DataFrame(np.array([lasso_reg_dem.coef_]),
                                      columns=dfdem_factors.columns)
    fig, ax = plt.subplots(figsize=(22, 15))
    width = 0.3
    ax.bar(np.arange(len(lasso_reg_rep.coef_)), lasso_reg_rep.coef_, width,
           label='Republican Portfolio')
    ax.bar(np.arange(len(lasso_reg_rep.coef_)) - width, lasso_reg_dem.coef_, width,
           label='Democratic Portfolio')

    ax.set_xticks(np.arange(len(lasso_reg_rep.coef_)))
    ax.set_xticklabels(dfrep_factors.columns, rotation=90)
    ax.set_title('Lasso Regression on Debate Factors')
    ax.set_xlabel('Factor')
    ax.set_ylabel('Lasso Coefficient')
    ax.legend(loc='best')
    fig.savefig('/Users/rohitchandel/Documents/be_results/debate_lasso.png')
    