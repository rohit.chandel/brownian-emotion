import pandas as pd
import os
# local modules
import emotion
import emotion.lib.ts as ts
import emotion.lib.utils as utils
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever
from sklearn.linear_model import Lasso
import statsmodels.api as sm
import emotion.lib.tsa as tsa
import numpy as np
import matplotlib.pyplot as plt

def get_funding_factor(name, start, end):
    if '.dem' in name:
        dffund = pd.read_csv(os.path.join(emotion.ROOT_DIR,
                                            '../external_data/demo_funding_factor.csv'))
    else:
        dffund = pd.read_csv(os.path.join(emotion.ROOT_DIR,
                                            '../external_data/rep_funding_factor.csv')) 
    
    dfout = ts.date_filter(dfout, start, end)
    return dfout

@stash_to_hub
def get_stashd_debate_factor(name, start, end):
    return get_debate_factor(name, start, end)


def get(name, start_date, end_date, **kwargs):
    return get_stashd_debate_factor(name, start_date, end_date, **kwargs)


if __name__ == '__main__':
    start = pd.Timestamp('19991231', tz='UTC')
    end = pd.Timestamp('20200204', tz='UTC')
