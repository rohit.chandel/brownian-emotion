# core modules
import pandas as pd
import logging

# local modules
import emotion
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever

def get_portfolio(name, start_date, end_date):
    """ timeseries of portfolio holdings b/w start and end date
    """
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    cfg = emotion.config[name]
    price_source = cfg['price_source']
    dfprice = retriever.get(price_source, start_date, end_date)
    if '.democratic' in name:
        univ = emotion.config['universe']['democratic']
    elif'.republic' in name:
        univ = emotion.config['universe']['republican']
    else:
        return None
    if dfprice is None:
        return None
    dfportfolio = dfprice[univ].copy()
    dfportfolio[~dfportfolio.isnull()] = 1  # not nan
    dfportfolio = dfportfolio.div(dfportfolio.sum(axis=1), axis=0)
    dfportfolio = ts.date_filter(dfportfolio, start_date, end_date)
    return dfportfolio

@stash_to_hub
def get_stashed_portfolio(name, start_date, end_date, **kwargs):
    """ get timeseries of winning party singal
    """
    return get_portfolio(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_portfolio(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name = "emotion.genesis.portfolio.democratic"
    start_date = pd.Timestamp('19100101', tz='UTC')
    end_date = pd.Timestamp('20190101', tz='UTC')
    df = retriever.get(name, start_date, end_date)
