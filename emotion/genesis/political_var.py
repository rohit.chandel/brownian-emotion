import pandas as pd
import os
# local modules
import emotion
import emotion.lib.ts as ts
import emotion.lib.utils as utils
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever
from sklearn.linear_model import Lasso
import statsmodels.api as sm
import emotion.lib.tsa as tsa
import numpy as np
import matplotlib.pyplot as plt


def get_political_factors(name, start, end):
    dfpol = pd.read_csv(os.path.join(emotion.ROOT_DIR,
                                            '../external_data/political_var.csv'))
    dfpol['date'] = pd.to_datetime(dfpol['date'], utc=True)
    dfpol = dfpol.rename(columns={
            'date': 'DATE'
            })
    dfpol = dfpol.set_index('DATE')
    dfpol = dfpol.drop(['President','President Republican', 'Congress Majority Republican', 'Senate Majority Republican'], axis=1)
    dfpol = dfpol.asfreq('B', method='ffill')
    dfpres = retriever.get('emotion.raw.winningparty', start, end)
    dfout = pd.concat([dfpol, dfpres], axis=1)
    dfout = ts.date_filter(dfout, start, end)
    return dfout

@stash_to_hub
def get_stashd_political_factor(name, start, end):
    return get_political_factors(name, start, end)


def get(name, start_date, end_date, **kwargs):
    return get_stashd_political_factor(name, start_date, end_date, **kwargs)


if __name__ == '__main__':
    start = pd.Timestamp('19930131', tz='UTC')
    end = pd.Timestamp('20191231', tz='UTC')
    name = 'emotion.genesis.political_var'
    name_ccs = 'emotion.genesis.ccs.factor'
    df = retriever.get(name_ccs, start, end)
    dfccs_factor = df[['agg_score']]
    df_nominate = df[['p0']]
    df_nominate = df_nominate.rename(columns={
            'p0': 'DW_Nominate'
            })
    df = retriever.get(name, start, end)
    dfpol = df.asfreq(freq='BM', method='ffill')
    df_nominate = df_nominate.asfreq(freq='BM', method='ffill')
    dfpol = dfpol[['House Gridlock', 'House-President Gridlock', 'Democratic']]
    signal_analytics = tsa.TSA(label='emotion.genesis.campaign_factor')
    dfrep = retriever.get('emotion.genesis.portfolio.republican', start, end)
    dfdem = retriever.get('emotion.genesis.portfolio.democratic', start, end)
    dfh_rep = signal_analytics.get_holdings(dfrep)
    dfh_dem = signal_analytics.get_holdings(dfdem)
    dfret = retriever.get('emotion.raw.return', start, end )
    df_dem_ret = signal_analytics.get_returns(dfh_dem, dfret)
    df_rep_ret = signal_analytics.get_returns(dfh_rep, dfret)
    df_dem_ret = (1+df_dem_ret).cumprod().resample('BM').last().pct_change().dropna()
    df_rep_ret = (1+df_rep_ret).cumprod().resample('BM').last().pct_change().dropna()

    dffundamental = retriever.get('emotion.raw.factor.fundamental', start, end)
    dffundamental = dffundamental[['Mkt-RF', 'SMB', 'HML', 'CMA', 'RMW']]
    dffundamental = (1+dffundamental).cumprod().resample('BM').last().pct_change().dropna()
    dfcampaign = retriever.get('emotion.genesis.campaign_factor', start, end)
    dfcampaign = dfcampaign.reindex(dfpol.index, method='ffill')
#    dfrep_factors = dfcombined_factors
#    dfdem_factors = dfcombined_factors
    # dem ols regression
    dfy_dem = df_dem_ret.reindex(df_nominate.index)
    dffundamental_dem = dffundamental.reindex(df_nominate.index)
    dfdem_factors = pd.concat([dffundamental, df_nominate], axis=1)
    dfdem_factors = dfdem_factors.reindex(dfy_dem.index)
    dem_res = sm.OLS(dfy_dem, dfdem_factors).fit()
    
    
    # rep ols regression
    dfy_rep = df_rep_ret.reindex(df_nominate.index)
    dffundamental_rep = dffundamental.reindex(df_nominate.index)
    dfrep_factors = pd.concat([dffundamental, df_nominate], axis=1)
    dfrep_factors = dfrep_factors.reindex(dfy_rep.index)
    rep_res = sm.OLS(dfy_rep, dfrep_factors).fit()
    
    out_res = sm.OLS(dfy_dem - dfy_rep, dfdem_factors).fit()
    
    
    
    