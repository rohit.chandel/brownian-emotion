"""
Stock wise monthly factor exposure(fama french and 5 industries)
"""

# core modules
import pandas as pd
import logging
# local modules
import emotion
import emotion.lib.ts as ts

from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever



def compute_factor_exp(name, start, end):
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start.strftime('%Y-%m-%d'),
                      end.strftime('%Y-%m-%d')))
    cfg = emotion.config[name]
    dffactor = retriever.get(cfg['factor_source'], start, end)
    dfret = retriever.get(cfg['ret_source'], start, end)
    dffactor = dffactor[['Mkt-RF', 'RF']]
    dfbetas = {}
    for symbol in dfret.columns:
        dfx = dffactor.copy()
        dfy = dfret[[symbol]] - dfx[['RF']].rename(columns={'RF': symbol})
        dfx = dfx.drop(['RF'], axis=1)
        dfx = dfx.loc[dfy[dfy[symbol].notnull()].index]
        dfbeta, _ = ts.rolling_regress(dfy, dfx, step=30, window=750,
                model='OLS',
                min_period=750,
                fill_value=None,
                fit_intercept=True)
        if dfbeta is None:
            continue
        dfbeta = dfbeta.drop(['const'], axis=1)
        dfbetas[symbol] = dfbeta
    
    dfbetas = pd.concat(dfbetas, axis=1)
    dfbetas = dfbetas.asfreq('D').ffill()
    dfbetas = dfbetas.asfreq('M').ffill()
    return dfbetas

@stash_to_hub
def get_stashed_exp(name, start_date, end_date, **kwargs):
    """ get 3D timeseries stock wise factor exposure
    """
    return compute_factor_exp(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_exp(name, start_date, end_date, **kwargs)

if __name__ == '__main__':
    name = 'emotion.genesis.factor_exp'
    start = pd.Timestamp('19721231', tz='UTC')
    end = pd.Timestamp('20200131', tz='UTC')
    df = retriever.get(name, start, end)
    
    
    