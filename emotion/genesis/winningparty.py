"""
Signal of going long of winning party portfolio and short of
losing party portfolio
"""
# core modules
import pandas as pd
import logging
import numpy as np

# local modules
import emotion
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever

def compute_signal(name, start_date, end_date):
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    cfg = emotion.config[name]
    winner_source = cfg['party_source']
    dfwinner = retriever.get(winner_source, start_date, end_date)
    dfwinner = dfwinner.astype(np.float)
    if '.longshort' in name:
        dfwinner[dfwinner==1] = -1.0
        dfwinner[dfwinner==0] = 1.0

    dfsig = pd.DataFrame()
    democrat_univ = emotion.config['universe']['democratic']
    republican_univ = emotion.config['universe']['republican']
    for symbol in democrat_univ:
        dfsig[symbol] = dfwinner['Democratic']
    for symbol in republican_univ:
        dfsig[symbol] = dfwinner['Republican']
    if dfsig is None:
        return None
    dfsig = ts.date_filter(dfsig, start_date, end_date)
    return dfsig


@stash_to_hub
def get_stashed_winningparty(name, start_date, end_date, **kwargs):
    """ get timeseries of winning party singal
    """
    return compute_signal(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_winningparty(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name = "emotion.genesis.winningparty.longshort"
    start_date = pd.Timestamp('19100101', tz='UTC')
    end_date = pd.Timestamp('20190101', tz='UTC')
    df = retriever.get(name, start_date, end_date)