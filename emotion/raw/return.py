"""
Compute daily returns from raw prices
"""

# core modules
import pandas as pd
import logging
# local modules
import emotion
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever


def compute_returns(name, start_date, end_date):
    """ workhorse method to compute returns from prices
    """
    cfg = emotion.config[name]
    src_name = cfg['source']
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    # get extra data to avoid nans on the first date
    offset = pd.offsets.BDay(5)
    dfprice = retriever.get(src_name, start_date-offset, end_date)
    dfret = dfprice.pct_change(1)
    if dfret is None:
        return None
    # make sure we don't generate outside of req dates
    dfret = ts.date_filter(dfret, start_date, end_date)
    return dfret


@stash_to_hub
def get_stashed_returns(name, start_date, end_date, **kwargs):
    """ get timeseries of returns
    """
    return compute_returns(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_returns(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name = "emotion.raw.return"
    start_date = pd.Timestamp('20100101', tz='UTC')
    end_date = pd.Timestamp.now(tz='UTC')
    df = retriever.get(name, start_date, end_date)
    
    
    

    
