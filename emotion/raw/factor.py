"""
Raw parser for factors

Currently we have 10 industry factors, 5 fama french factors and economic
factors.

"""

# core modules
import logging
import pandas as pd
import pandas_datareader.data as web

# local modules
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever

def parse_fundamental_factor_returns(name, start_date, end_date):
    """ parse fundamental factor returns
    """
    # ken french update date
    last_updated_date = pd.Timestamp('20191231', tz='UTC')
    if start_date > last_updated_date:
        return None
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    # read 5 fama french factors returns
    ds = web.DataReader('F-F_Research_Data_5_Factors_2x3_daily', 'famafrench',
                        start=start_date, end=end_date)
    dfff = ds[0]
    # read industry factors returns
    ds = web.DataReader('5_Industry_Portfolios_Daily', 'famafrench',
                        start=start_date, end=end_date)
    dfind = ds[0]
    # combine into one ts
    dffactor = pd.concat([dfff, dfind], axis=1)
    dffactor.columns.set_names(['FACTORS'], inplace=True)
    dffactor.index = pd.to_datetime(dffactor.index, utc=True)
    dffactor.index = dffactor.index.rename(ts.DATENAME)
    dffactor = dffactor.loc[~dffactor.index.duplicated(keep='first')]
    # convert to decimal
    dffactor = dffactor/100
    # always add this operation
    dffactor = ts.date_filter(dffactor, start_date, end_date)
    return dffactor


def parse_economic_factors(name, start_date, end_date):
    """ parse economical factors
    """
    return None

@stash_to_hub
def get_stashed_portfolio(name, start_date, end_date, **kwargs):
    """ get timeseries of winning party singal
    """
    if '.economic' in name:
        return parse_economic_factors(name, start_date, end_date)
    elif '.fundamental' in name:
        return parse_fundamental_factor_returns(name, start_date, end_date)
        
    
def get(name, start_date, end_date, **kwargs):
    return get_stashed_portfolio(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name_economic = "emotion.raw.factor.economical"
    name = "emotion.raw.factor.fundamental"
    start_date = pd.Timestamp('20140101', tz='UTC')
    end_date = pd.Timestamp('20181230', tz='UTC')
    dfx = retriever.get(name, start_date, end_date)
    