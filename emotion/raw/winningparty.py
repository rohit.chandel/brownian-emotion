"""
Timeseries of winning party. 1 is  winner and 0 is loser at a point in time
"""
# core modules
import pandas as pd
import logging
import numpy as np

# local modules
import emotion
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever

def parse_winning_party(name, start_date, end_date):
    """ workhorse method to winning party time series since utils.__ORIGIN__
    """
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    cfg = emotion.config[name]
    src_name = cfg['source']
    dfraw = pd.read_csv(src_name)
    dfraw = dfraw.rename(columns = lambda x: x.strip())
    for col in dfraw.select_dtypes(include='object').columns:
        dfraw[col] = dfraw[col].str.strip()
    # update barack's row
    dfraw.loc[dfraw['President'] == 'Barack Obama',
              'Left office'] = '20/01/2017'
    # add trump's row
    
    dfraw = dfraw.append({
            'President': 'Donald Trump',
            'Party': 'Republican',
            'Took office': '20/01/2017',
            'Left office': np.nan
            }, ignore_index=True)
    dfraw['Took office'] = pd.to_datetime(dfraw['Took office'], utc=True)
    dfraw['Left office'] = pd.to_datetime(dfraw['Left office'], utc=True)
    dfraw = dfraw[['Took office', 'Party']]
    dfraw = dfraw[dfraw['Took office'] > '1950']
    dfraw = pd.get_dummies(dfraw, prefix='', prefix_sep='',
                           columns=['Party'])
    dfraw = dfraw.rename(columns={'Took office': ts.DATENAME})
    dfraw = dfraw.set_index(ts.DATENAME)
    pdates = pd.date_range(start_date, end_date, freq='B')
    dfraw = dfraw.reindex(pdates, method='ffill')
    if dfraw is None:
        return None
    dfraw.index.set_names(['DATE'], inplace=True)
    return dfraw


@stash_to_hub
def get_stashed_winningparty(name, start_date, end_date, **kwargs):
    """ get timeseries of winning party
    """
    return parse_winning_party(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_winningparty(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name = "emotion.raw.winningparty"
    start_date = pd.Timestamp('19100101', tz='UTC')
    end_date = pd.Timestamp('20190101', tz='UTC')
    df = retriever.get(name, start_date, end_date)
    
    
    

    

