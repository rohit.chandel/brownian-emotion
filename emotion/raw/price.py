"""
Parse raw prices from yahoo finance
"""
# core modules
import pandas as pd
import yfinance as yf
import logging

# local modules
import emotion.lib.ts as ts
from emotion.lib.utils import stash_to_hub
import emotion.lib.retriever as retriever


def download_prices(name, start_date, end_date):
    table=pd.read_html('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
    dfsnp = table[0]
    omni = list(dfsnp['Symbol']) + ['FSLR', 'SPWR']
    dflist = []
    logging.getLogger(name).info('Generating %s between %s and %s' %
                     (name, start_date.strftime('%Y-%m-%d'),
                      end_date.strftime('%Y-%m-%d')))
    for symbol in omni:
        dfprice = yf.download(symbol, start_date.strftime('%Y-%m-%d'),
                       end_date.strftime('%Y-%m-%d'))
        # change to utc
        dfprice.index = pd.to_datetime(dfprice.index, utc=True)
        cols = dfprice.columns
        dfprice[symbol] = dfprice['Adj Close']
        dfprice = dfprice.drop(cols, axis=1)
        if dfprice is not None:
            dfprice = dfprice.loc[~dfprice.index.duplicated(keep='first')]
            dflist.append(dfprice)
    dfprices = pd.concat(dflist, axis=1)
    dfprices.index.set_names([ts.DATENAME], inplace=True)
    if dfprices is None:
        return None
    dfprices = ts.date_filter(dfprices, start_date, end_date)
    return dfprices

    
@stash_to_hub
def get_stashed_prices(name, start_date, end_date, **kwargs):
    """
    function does not regenrate data if it already exists
    """
    return download_prices(name, start_date, end_date)
    

def get(name, start_date, end_date, **kwargs):
    return get_stashed_prices(name, start_date, end_date, **kwargs)
    
    
if __name__ == "__main__":
    name = "emotion.raw.price"
    start_date = pd.Timestamp('20100101', tz='UTC')
    end_date = pd.Timestamp.now(tz='UTC')
    df = retriever.get(name, start_date, end_date)
    
    
    

    
