import os
import logging
import emotion
import pandas as pd
import emotion.lib.backtester as backtester

import emotion.lib.retriever as retriever
import emotion.lib.utils as utils

if __name__ == '__main__':
    if not os.path.exists(os.path.join(emotion.ROOT_DIR, '../hub')):
        os.makedirs(os.path.join(emotion.ROOT_DIR, '../hub'))
    if not os.path.exists(os.path.join(emotion.ROOT_DIR, '../empirics')):
        os.makedirs(os.path.join(emotion.ROOT_DIR, '../empirics'))
    offset = pd.offsets.BDay(60)
    cfg = emotion.config
    items = cfg.keys()
#    items = [item  for item in items if '.raw.' in item or '.genesis.' in item]
#    for item in items:
#        logging.getLogger('MAIN').info('Genrating data item %s.' % item)
#        retriever.get(item, start_date=utils.__ORIGIN__,
#                      end_date=pd.Timestamp.now(tz='UTC') - offset)
        
#    signals = ['emotion.genesis.winningparty.long',
#              'emotion.genesis.winningparty.longshort',
#              'emotion.genesis.portfolio.democratic',
#              'emotion.genesis.portfolio.republican']
    signals = [
              'emotion.genesis.winningparty.longshort'
              ]
    for signal in signals:
        backtester.run(signal)
        logging.getLogger('MAIN').info('Backtesting results generated '
                         'for signal %s.' % signal)