"""
module to signal data dynamically using name of the data item
"""
# core modules
import os
import importlib
import pandas as pd
import emotion
from emotion.lib.error import ExceptionalEmotion


def get(name, start_date=pd.Timestamp.now(tz='UTC'),
        end_date=pd.Timestamp.now(tz='UTC'), stashed_only=False):
    """ access data between start_date and end_date for any module
    Args:
        name: name of the module
    Returns:
        pd.DataFrame: df 
    Raises:
        ExceptionalEmotion: error when data module does not exist.
    """
    module_structure = name.split('.')
    while module_structure:
        file_name = os.path.join(emotion.ROOT_DIR,
                                 '../'+'/'.join(module_structure)+'.py')
        if os.path.isfile(file_name):
            break
        module_structure = module_structure[:-1]
    if not module_structure:
        raise ExceptionalEmotion("name %s does not exist." % name)
    
    module = importlib.import_module('.'.join(module_structure))
    if not hasattr(module, 'get'):
        raise ExceptionalEmotion("Module does not have get method." )
    return module.get(name, start_date, end_date, stashed_only=stashed_only)
