"""
Utilities
"""
import pandas as pd
import os
import emotion

import emotion.lib.ts as ts


__ORIGIN__ = pd.Timestamp('19691231', tz='UTC')

def stash_to_hub(func):
    """ stashing decorator func.
    function stashes to hub in pickle format. Saves computation cost of 
    regenerating data if item is already stashed.
    """
    def wrapped_func(name,
                     start_date,
                     end_date, stashed_only=False):
        cfg = emotion.config
        start_date_copy = start_date
        end_date_copy = end_date
        hub_path = os.path.join(emotion.ROOT_DIR, cfg['hub_path'])
        pkl_name = name + '.pkl'
        files = os.listdir(hub_path)
        dfexisting = None
        if pkl_name in files:
            dfexisting = pd.read_pickle(os.path.join(hub_path, pkl_name))
            # start generating from last updated date
            start_date = dfexisting.index[-1] + pd.offsets.BDay(1)
        else:
            # if nothing is generated start generating from 1969
            start_date = __ORIGIN__
        if stashed_only == True:
            return dfexisting
        dfnew = None
        if start_date < end_date:
            dfnew = func(name, start_date, end_date)
        dfout = []
        if dfexisting is not None:
            dfout.append(dfexisting)
        # make sure no 
        if dfnew is not None:
            dfnew = ts.date_filter(dfnew, start_date, end_date)
        if dfnew is not None:
            dfout.append(dfnew)
        if not dfout:
            return None
        dfout = pd.concat(dfout)
        dfout = dfout.dropna(axis=0, how='all')
        if dfout is None:
            return None
        dfout = dfout.dropna(axis=1, how='all')
        if dfout is None:
            return None
        dfout.to_pickle(os.path.join(hub_path, pkl_name))
        dfout = ts.date_filter(dfout, start_date_copy, end_date_copy)
        return dfout
    return wrapped_func