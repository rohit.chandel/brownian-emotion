"""
Frequencty used operations on timeseries dataframe. Time series is defined
as df widh index as pd.Timestamp and  name "DATE" 
"""
# core modules
import logging
import numpy as np
import pandas as pd
# lasso with cross validation
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression
import statsmodels.api as sm
from emotion.lib.error import ExceptionalEmotion

# label of the index of timeseries index. This is made public so that consumer 
# can use it while renameing index.
DATENAME = 'DATE'

def rolling_mean(df,
                window=500,
                min_periods=None,
                ):
    """
    Computes rolling mean
    Args:
        df(pd.DataFrame): ts dataframe
        min_period(int): min periods to compute mean
    Returns:
        pd.DataFrame: mean dataframe
    """
    if not is_ts(df):
        raise ExceptionalEmotion('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).mean()
    return df
   
    
def rolling_std(df,
                window=500,
                min_periods=None,
                ):
    """
    Computes rolling std
    Args:
        df(pd.DataFrame): ts dataframe
        window(int): rolling window size
        min_period(int): min periods to compute std
    Returns:
        pd.DataFrame: std dataframe
    """
    if not is_ts(df):
        raise ExceptionalEmotion('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).std()
    return df


def rolling_tscore(df,
           window=500,
           min_periods=None
           ):
    """
    Standardize the time series to be
    with mean 0 and std 1 in rolling window
    Args:
        df(pd.DataFrame): ts dataframe to standardise
        window(int): rolling window size
        min_period(int): min periods to compute mean and std
    Returns:
        pd.DataFrame: trimmed dataframe
    """
    if not is_ts(df):
        raise ExceptionalEmotion('df should be valid time series dataframe')
    df = df.copy()
    dfmean = rolling_mean(df,
                          window=window,
                          min_periods=min_periods)
    dfstd = rolling_std(df,
                        window=window,
                        min_periods=min_periods) 
    # filtering to get min periods functionality
    # borrowed from pandas
    df = df[dfmean.notnull()]
    df = (df - dfmean).div(dfstd)
    return df


def rolling_tclip(df,
                  threshold=3,
                  window=500,
                  min_periods=None
                  ):
    """
    Removes the outlier in ts in rolling window
    above given threshold and clip to the threshold value
    Args:
        df(pd.DataFrame): ts dataframe to trim
        threshold(int): outlier removal threshold
        window(int): rolling window size
        min_period(int): min periods to compute mean and std
    Returns:
        pd.DataFrame: trimmed dataframe
    """
    if not is_ts(df):
        raise ExceptionalEmotion('df should be valid time series dataframe')
    df = df.copy()
    dfmean = rolling_mean(df,
                          window=window,
                          min_periods=min_periods)
    dfstd = rolling_std(df,
                        window=window,
                        min_periods=min_periods)
    # filtering to get min periods functionality
    # borrowed from pandas
    df = df[dfmean.notnull()]
    pos_threshold = dfmean + threshold*dfstd
    neg_threshold = dfmean - threshold*dfstd
    pos_outlier_mask = (df > pos_threshold)
    neg_outlier_mask  = (df < neg_threshold)
    # clip values
    df[pos_outlier_mask] = pos_threshold
    df[neg_outlier_mask] = neg_threshold
    return df

    
def is_ts(df):
    """
    checks if df is a valid time series
    key checks - DATE index
    Args:
        df(pd.DataFrame): dataframe to check
    Returns:
        boolean
    Raises:
        GenesisError
    """
    if DATENAME != df.index.name:
        return False
    if not isinstance(df.index, pd.DatetimeIndex):
        return False
    return True

def regress(dfy, dfx, model='OLS', 
            fill_value=None,
            **kwargs):
    """
    lasso regression or ols regression
    Args:
        dfy(pd.DataFrame): time series of y variable
        dfx(pd.DataFrame): time series of x variables
    Returns:
        np.array: list of coeffs
        float: score       
    """
    if model == 'OLS':
        model_class = LinearRegression
    elif model == 'Lasso':
        model_class = Lasso
    else:
        raise ExceptionalEmotion('Unknow model. Pass OLS or Lasso')

    if not is_ts(dfy) or not is_ts(dfx):
        raise ExceptionalEmotion('dfx and dfy should valid time series dataframes')
    if fill_value is not None: 
        dfy = dfy.fillna(fill_value)
        dfx = dfx.fillna(fill_value)
    else:
        logging.getLogger('LassoCV').warning(
                "Dropping features rows with missing values.")
        dfy = dfy.dropna()
        dfx = dfx.dropna()
    reg = model_class(**kwargs).fit(dfx, dfy.values.ravel())
    return reg


def rolling_lassso(dfy, dfx, step=30, window=750,
                    model='OLS',
                    min_period=None,
                    fill_value=None,
                    fit_intercept=True, **kwargs):
    """
    Rolling lasso regression or ols regression
    with given window and step size
    Loss uses cross validation to find the optimal alpha penalty
    
    Args:
        dfy(pd.DataFrame): time series of y variable
        dfx(pd.DataFrame): time series of x variables
        step(pd.DataFrame): step size
        model('str'): model to fit(OLS)
        window(int): window size
        min_period(int): minium periods required for regression
        fill_value(int): fill nans with this value
        fit_intercept(bool): fit intercept in regression,
    Returns:
        pd.DataFrame: df of beta coeffcients
        pd.DataFrame: df of scores        
    """
    if not is_ts(dfy) or not is_ts(dfx):
        raise ExceptionalEmotion('dfx and dfy should valid time series dataframes')
    if fill_value is not None: 
        dfy = dfy.fillna(fill_value)
        dfx = dfx.fillna(fill_value)
    else:
        logging.getLogger('Lasso').warning(
                "Dropping features rows with missing values.")
        dfy = dfy.dropna()
        dfx = dfx.dropna()
    dfbetalist = []
    dfscoreslist = []
    for i in range(window, dfx.shape[0], step):
        dfxcurr = dfx.iloc[i-window: i]
        dfycurr = dfy.iloc[i-window: i]
        curr_reg = Lasso(**kwargs).fit(dfxcurr, dfycurr.values.ravel())
        dfcurrbeta = pd.DataFrame(np.array([curr_reg.coef_]),
                                  columns=dfx.columns)
        score = curr_reg.score(dfxcurr, dfycurr.values.ravel())
        dfcurrbeta[DATENAME] = dfx.index[i]
        dfscore = pd.DataFrame({'score': [score]})
        dfscore[DATENAME] = dfx.index[i]
        dfscoreslist.append(dfscore)
        dfbetalist.append(dfcurrbeta)
    
    if len(dfbetalist)==0 :
        return None, None
    dfbeta = pd.concat(dfbetalist)
    dfscores = pd.concat(dfscoreslist)
    dfbeta = dfbeta.set_index(DATENAME)
    dfscores = dfscores.set_index(DATENAME)
    return dfbeta, dfscores


def rolling_regress(dfy, dfx, step=30, window=750,
                    model='OLS',
                    min_period=750,
                    fill_value=None,
                    fit_intercept=True):
    """
    Rolling lasso regression or ols regression
    with given window and step size
    Loss uses cross validation to find the optimal alpha penalty
    
    Args:
        dfy(pd.DataFrame): time series of y variable
        dfx(pd.DataFrame): time series of x variables
        step(pd.DataFrame): step size
        model('str'): model to fit(OLS)
        window(int): window size
        min_period(int): minium periods required for regression
        fill_value(int): fill nans with this value
        fit_intercept(bool): fit intercept in regression,
    Returns:
        pd.DataFrame: df of beta coeffcients
        pd.DataFrame: df of scores        
    """
    if model == 'OLS':
        model_class = sm.OLS
    else:
        raise ExceptionalEmotion('Unknown model. Pass OLS')

    if not is_ts(dfy) or not is_ts(dfx):
        raise ExceptionalEmotion('dfx and dfy should valid time series dataframes')
    if fill_value is not None: 
        dfy = dfy.fillna(fill_value)
        dfx = dfx.fillna(fill_value)
    else:
        logging.getLogger(model).warning("Dropping features rows with missing values.")
        dfy = dfy.dropna()
        dfx = dfx.dropna()
    if fit_intercept:
        dfx = sm.add_constant(dfx)
    dfbetalist = []
    dftstatlist = []
    for i in range(window, dfx.shape[0], step):
        dfxcurr = dfx.iloc[i-window: i]
        dfycurr = dfy.iloc[i-window: i]
        curr_reg = model_class(dfycurr, dfxcurr).fit()
        dfcurrbeta = pd.DataFrame(np.array([curr_reg.params]),
                                  columns=dfx.columns)
        tstat = curr_reg.params/curr_reg.bse
        dfcurrbeta[DATENAME] = dfx.index[i]
        dfcurrtstat = pd.DataFrame(np.array([tstat]),
                                  columns=dfx.columns)
        dfcurrtstat[DATENAME] = dfx.index[i]
        dftstatlist.append(dfcurrtstat)
        dfbetalist.append(dfcurrbeta)
    if len(dfbetalist)==0 :
        return None, None
    dfbeta = pd.concat(dfbetalist)
    dftstat = pd.concat(dftstatlist)
    dfbeta = dfbeta.set_index(DATENAME)
    dftstat = dftstat.set_index(DATENAME)
    return dfbeta, dftstat



def dot(dfa, dfb):
    """
    row wise dot product of two time series
    """
    if not is_ts(dfa) or not is_ts(dfb):
        raise ExceptionalEmotion('dfa and dfb should valid time series dataframes')
    return dfa.mul(dfb).sum(axis=1) 



def date_filter(df, start_date, end_date):
    """
    filter ts between start and end dates
    """
    if not is_ts(df):
        raise ExceptionalEmotion('Invalid timeseries')
    return df.loc[(df.index >= start_date) & (df.index <= end_date)]
    

if __name__ == '__main__':
    #for testing
    start_time = pd.Timestamp('20190610', tz='UTC')
    end_time = pd.Timestamp('20190614', tz='UTC')

        
        
    