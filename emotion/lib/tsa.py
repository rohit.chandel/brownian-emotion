"""
Time series analysis module to  generate backtests.
This module will also be used by back tester for
doing time series analaysis of signals, holdings and returns.
"""
# core modules 
import os
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# genesis module
import emotion
import emotion.lib.retriever as retriever
import emotion.lib.utils as utils
import emotion.lib.ts as ts
import matplotlib.style as style 

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')
matplotlib.rcParams['font.family'] = "serif"

class TSA:
    """ TSA class to test signal results
    """
    def  __init__(self, label,
                plotter='matplotlib'):
        """
        Constructor
        Args:
            label(str): label to be used for plotting
            plotter(str): matplotlib or None if plots are not required
            plotDir(str): path to output result dir
        """
        self.label = label
        self.plotter = plotter
        self.plotdir = os.path.join(emotion.ROOT_DIR,
                                    emotion.config['empirics_path'],
                                    label)
        if not os.path.isdir(self.plotdir):
            os.mkdir(self.plotdir)        
        
    def get_holdings(self, dfsig):
        """ simple holding optimizer to get basic holdings.
        for political stategy, it will be just ts of 1 and -1
        """
        df = dfsig.copy()
        df[df > 0] = df[df > 0].div(df[df > 0].sum(axis=1), axis=0)
        df[df < 0] = df[df < 0].div(df[df < 0].sum(axis=1).abs(), axis=0)
        return df
        
    
    def get_returns(self, dfret, dfh):
        """ returns from portfolio
        """
        dfport_ret = dfh.mul(dfret).sum(axis=1)
        dfport_ret = dfport_ret.to_frame(name=self.label)
        return dfport_ret
    
    
    def cum_ret(self, dfret):
        """ get the cumulative returns
        """
        dfcumret = (1+dfret).cumprod(axis=0) - 1
        ax = dfcumret.plot()
        ax.set_title('%s.cumulative_ret' % (self.label))
        fig = ax.get_figure()
        fig.savefig(os.path.join(self.plotdir, self.label+'.cumret.png'))        
        return dfcumret
    
    def ir(self, dfret, lead_lags=range(-15, 15)):
        """ get lead and lag ir
        """
        dflead_lag = dfret.copy()
        for i in lead_lags:
            dflead_lag[i] = dfret.shift(i)
        dflead_lag = dflead_lag[lead_lags]
        dfir = dflead_lag.mean()*(252**0.5)/dflead_lag.std()
        fig, ax = plt.subplots()
        ax.bar(dfir.index, dfir.values)
        ax.set_title('ir_lead_lags')
        ax.set_ylabel('IR')
        ax.set_xlabel('lead_lags')
        fig.savefig(os.path.join(self.plotdir, self.label+'.ir.png'))        
        return dfir
    
    def kde(self, dfret):
        fig, ax = plt.subplots()
        dfret.hist(ax=ax, bins=50)
        ax2 = dfret.plot.kde(ax=ax, secondary_y=True, title='Returns Distribution')
        ax2.set_ylim(0)
        fig.savefig(os.path.join(self.plotdir, self.label+'.kde.png'))
          
    def drawdown(self, dfret):
        dfcumret = self.cum_ret(dfret)
        dfval = 1 + dfcumret
        dfdrawdown = (dfval/dfval.cummax()) - 1
        fig, ax = plt.subplots()
        dfdrawdown.plot(ax=ax)
        fig.savefig(os.path.join(self.plotdir, self.label+'.drawdown.png'))
        
    def rolling_exposure(self, dfret):
        """
        get rolling exposure to risk factors
        """
        dfx = retriever.get('emotion.raw.factor.fundamental',
                           start_date=utils.__ORIGIN__)
        dfy = dfret - dfx[['RF']].rename(columns={'RF': dfret.columns[0]})
        dfx = dfx.drop(['RF'], axis=1)
        dfy = dfy.reindex(dfx.index)
        dfbeta, dftstat = ts.rolling_regress(dfy, dfx, step=60, window=1000,
                    model='OLS',
                    min_period=1000,
                    fill_value=None,
                    fit_intercept=True)
        
        dfbeta = dfbeta.drop(['const'], axis=1)
        dftstat = dftstat.drop(['const'], axis=1)
        fig, ax = plt.subplots(figsize=(15,15))
        ax.set_title('Factor Exposure')
        ax.set_ylabel('Exp')
        ax.set_xlabel('Time')
        dfbeta.plot(ax=ax)
        fig.savefig(os.path.join(self.plotdir, self.label+'.exp.png')) 
        fig, ax = plt.subplots(figsize=(15,15))
        ax.set_title('Factor Exposure tstat')
        ax.set_ylabel('Exp tstat')
        ax.set_xlabel('Time')
        dftstat[np.abs(dftstat)>2].plot(ax=ax)
        fig.savefig(os.path.join(self.plotdir, self.label+'.exp_tstat.png'))
        
        
        dfbeta = dfbeta[dfbeta.index > '2010']
        dftstat = dftstat[dftstat.index > '2010']
        fig, ax = plt.subplots(figsize=(15,15))
        ax.set_title('Factor Exposure after 2010')
        ax.set_ylabel('Exp')
        ax.set_xlabel('Time')
        dfbeta.plot(ax=ax)
        fig.savefig(os.path.join(self.plotdir, self.label+'.exp2010.png')) 
        fig, ax = plt.subplots(figsize=(15,15))
        ax.set_title('Factor Exposure tstat after 2010')
        ax.set_ylabel('Exp tstat')
        ax.set_xlabel('Time')
        dftstat[np.abs(dftstat)>2].plot(ax=ax)
        fig.savefig(os.path.join(self.plotdir, self.label+'.exp_tstat2010.png'))
        
      