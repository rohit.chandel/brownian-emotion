"""
Backtesting module
"""
# core modules
import logging
import pandas as pd
# genesis modules
import emotion.lib.tsa as tsa
import emotion.lib.utils as utils
import emotion.lib.retriever as retriever


def run(signame):
    # get signal time series
    logger = logging.getLogger(signame)
    logger.info( 'Generating empirics for signal %s...' % (signame))
    offset = pd.offsets.BDay(55)
    dfsignal = retriever.get(signame, start_date=utils.__ORIGIN__,
                             end_date=pd.Timestamp.now(tz='UTC') - offset)
    # perform analysis
    signal_analytics = tsa.TSA(label=signame)
    dfh = signal_analytics.get_holdings(dfsignal)
    dfret = retriever.get('emotion.raw.return', start_date=utils.__ORIGIN__,
                             end_date=pd.Timestamp.now(tz='UTC') - offset)
    dfret = signal_analytics.get_returns(dfh, dfret)
    signal_analytics.cum_ret(dfret)
    signal_analytics.ir(dfret)
    signal_analytics.kde(dfret)
    signal_analytics.rolling_exposure(dfret)
    signal_analytics.drawdown(dfret)
